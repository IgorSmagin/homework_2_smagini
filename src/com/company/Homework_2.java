package com.company;
import java.io.*;

/**
 * Created by klient on 26.09.2016.
 */
public class Homework_2 {
    public static void main(String[] args) throws Exception {
        int min = minOfThree(9, 6, 5);
         System.out.println(min);
        long fac1 = factW(4);
        System.out.println(fac1);
        long fac2 = factdW(2);
        System.out.println(fac2);
        long fac3 = factf(6);
        System.out.println(fac3);
        massiv3();
        massiv4();
        massiv6_1();
        massiv6_2();
        massiv7();
        massiv8();
        massiv5();



    }



    public static int minOfThree(int a, int b, int c) throws Exception {
        int min = a;
        if (min > b) min = b;
        if (min > c) min = c;
        return min;

    }

    public static long factW (int a)throws Exception {
        long c = 1;
        int b = 1;
        while (b < a + 1) {
            c*= b;
            b++;
        }
        return c;


    }

    public static long factdW (int a)throws Exception {
        long c = 1;
        int b = 1;
        do {
            c*=b;
            b++;
        }
        while (b<a+1);
        return c;
    }

    public static long factf (int a)throws Exception {
        long b=1;
        for ( int i=1; (i<a+1); i++ ){
            b*=i;

        }
        return b;
    }

    public static void massiv3() throws Exception {
        int arr[]= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        for (int i =0; i<10; i++ )  System.out.print (arr[i]) ;

    }

    public static void massiv4() throws Exception {
        int arr[]= {1,4,5,6,5,4,12,5,3,5,-6,4};
        for (int i=0; i<arr.length; i++ ){
            if (arr[i]!=5) System.out.println(arr[i]);
        }
    }
    public static void massiv6_1() throws Exception {
        int arr[]= {4,-13,-6,3,11,2};
        int min=arr[1], max=arr[1];
        for (int i=0; i<arr.length; i++){
            if (min>arr[i]) min=arr[i];
            if (max<arr[i]) max=arr[i];
        }
        System.out.println("min= " +min +" max= " +max );

    }

    public static void massiv6_2() throws Exception {
        int arr[][]={{3,-11,2},{-5,9,4},{-12,-3,7}};
        int min=arr[1][1], max=arr[1][1];
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                if (min>arr[i][j]) min=arr[i][j];
                if (max<arr[i][j]) max=arr[i][j];
            }
        }
        System.out.println("min= " +min +" max= " +max );


    }

    public static void massiv7() throws Exception{
        int arr[][]={{3,-12,2},{-6,9,4},{-12,-3,7}};
        int sum=0;
            for (int i=0; i<3 ;i++){
            for (int j=0; j<i; j++){
                sum = sum+arr[i][j];
            }
        }
        System.out.println(sum);


    }

    public static void massiv8() throws Exception{
        int arr[]=new int[31];
        for (int i=0; i<31; i++) arr[i]=i+1;

        for (int i=0; i<arr.length; i++ ){
            if (arr[i]%7==5)
                    System.out.println(arr[i]);

        }
       System.out.println(arr[30]%7+" day of week");

    }

    public static void massiv5() throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        int a=15, b=3;
        switch (s) {
            case "+": System.out.println ("Result of plus operation is "+(a+b));
                break;
            case "-": System.out.println ("Result of minus operation is "+(a-b));
                break;
            case "*": System.out.println ("Result of mult operation is "+(a*b));
                break;
            case "/": System.out.println ("Result of div operation is "+(a/b));
                break;
            default:
                System.out.println("Unknown operation");

        }
    }
}
